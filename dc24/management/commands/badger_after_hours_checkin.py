from datetime import time

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import Template, Context
from django.utils.timezone import make_naive

from django.contrib.sites.models import Site

from register.models import Attendee


SUBJECT = '[DebConf 23]: After hours arrival, please double-check'
TXT = '''
Dear {{name}},

We see from our records that you are registered to arrive at the venue outside
front desk office hours (8am-6pm).

Arrival: {{ arrival }}

If this is correct, please contact the registration team, the day before you
arrive, to make a plan to check-in after hours.

If it is not correct, please update your registration appropriately:
<https://{{ WAFER_CONFERENCE_DOMAIN }}/register/>
And check that you have selected accommodation and meals for the correct dates.

Thanks,

The DebConf Registration Team
'''

OFFICE_HOURS = (time(8), time(18))


class Command(BaseCommand):
    help = "Badger attendees who said they are arriving after hours"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, attendee, dry_run, site):
        to = attendee.user.email
        name = attendee.user.get_full_name()

        arrival_time = make_naive(attendee.arrival).time()
        if OFFICE_HOURS[0] <= arrival_time <= OFFICE_HOURS[1]:
            return

        if dry_run:
            print(f'I would badger {name} <{to}>')
            return

        ctx = Context({
            'name': name,
            'arrival': attendee.arrival,
            'WAFER_CONFERENCE_DOMAIN': site.domain,
        })

        subject = Template(SUBJECT).render(ctx).strip()
        body = Template(TXT).render(ctx)

        email_message = EmailMultiAlternatives(subject, body,
                                               to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        site = Site.objects.get(id=1)
        for attendee in Attendee.objects.filter(
                arrival__isnull=False, departure__isnull=False):
            if not attendee.user.userprofile.is_registered():
                continue
            self.badger(attendee, dry_run, site)
